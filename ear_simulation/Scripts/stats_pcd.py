import vtk
import vtkutils
import os
import math
import numpy as np
from vtk.util import numpy_support
from matplotlib import pyplot as plt
import pickle


def calc_stats_pcd(folder, start, num, single_displ_plot=False, output_path=None):
    # pre_pcd_path = "/mnt/ceph/tco/TCO-Staff/Homes/liupeng/data/D2EAR_data/SSM/TMOSS3/TMOSS3_template.stl"
    pre_pcd_path = "/mnt/ceph/tco/TCO-Staff/Homes/liupeng/data/D2EAR_data/SimulationData/mean_model_vertex_reordered.stl"
    pre_pcd = vtkutils.loadMesh(pre_pcd_path)
    pre_pcd = numpy_support.vtk_to_numpy(pre_pcd.GetPoints().GetData())
    mean_displ_list = []
    displ_vector_mag_list = np.asarray([.0,] * pre_pcd.shape[0])
    for idx in range(start, start + num):
        cur_dir = os.path.join(folder, "{:06d}".format(idx))
        print("===============", cur_dir)
        intra_pcd_path = os.path.join(cur_dir, "intra_surface.stl")
        if os.path.exists(intra_pcd_path):
            intra_pcd = vtkutils.loadMesh(intra_pcd_path)
            intra_pcd = numpy_support.vtk_to_numpy(intra_pcd.GetPoints().GetData())
            print(intra_pcd.shape)
            displ = intra_pcd - pre_pcd
            displ_mag = np.linalg.norm(displ, axis=1)
            # print(np.linalg.norm(displ, axis=1).shape)
            mean_displ = np.mean(displ_mag)
            print("mean displ:", mean_displ)
            mean_displ_list.append(mean_displ)
            if not displ_vector_mag_list.any():
                displ_vector_mag_list = displ_mag
            else:
                displ_vector_mag_list += displ_mag 

            if single_displ_plot:
                plots(
                    stats=np.linalg.norm(displ, axis=1), 
                    output_folder=cur_dir,
                )

    displ_vector_mag_list /= num
    # mean_displ_list = np.asarray(mean_displ_list)
    print("min:", min(mean_displ_list), "max:", max(mean_displ_list), "mean:", sum(mean_displ_list)/len(mean_displ_list))
    if output_path:
        with open(output_path, "wb") as pkl_file:
            pickle.dump(mean_displ_list, pkl_file)
            pkl_file.close()
        print("saving mean_displ_list to:", output_path)
    return mean_displ_list, displ_vector_mag_list





def plots(stats,  output_folder="plots", filename=None):
    # step = 0.5 #0.03
    # x_start = 0.1
    # x_end = 3
    # box_num = int((x_end - x_start) / step)
    # print("box_num", box_num, "step", step)
    # dist_positions = np.arange(box_num).tolist()
    # dist_bins = [ [] for i in range(box_num + 1)]
    # for mean_displ in stats:
    #     i_x = int((mean_displ - x_start)/ step)
    #     # if i_x < box_num :
    #     dist_bins[i_x].append(mean_displ)

    plt.clf()
    plt.hist(stats, bins=70, edgecolor='black', linewidth=1.2)
    # plt.hist2d(dist_positions, dist_bins, cmap=plt.cm.jet)
    
    # plt.figure(figsize=(8 *0.7, 6 * 0.7))
    # plt.scatter(mde_initial_list, mde_list, )
    plt.xlabel("mean target displacement (mm)")
    plt.ylabel("number of samples")
    # plt.ylim(0, 3)
    # plt.grid(axis="y")
    if filename:
    # output_path = os.path.join(output_folder,  "mean_displ_hist2d.png")
        output_path = os.path.join(output_folder,  filename)
        # output_path_eps = output_path.replace(".png", ".eps")
        plt.savefig(output_path, bbox_inches='tight')
        # plt.savefig(output_path_eps, bbox_inches='tight')
        print("saved plot to:", output_path)
        # print("saved plot to:", output_path_eps)



if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description="Scripts used to calculate the stats of generated data")
    parser.add_argument("folder", type=str,  help="Folder where samples lie")
    parser.add_argument("--start", type=int,  default=0, required=False, help="Starting index of samples")
    parser.add_argument("--num", type=int,  default=1, required=False, help="The number of samples to be processed")

    args = parser.parse_args()
    folder = args.folder
    start = args.start
    num = args.num

        
    mean_displ_list, displ_vector_mag_list = calc_stats_pcd(
        folder=folder, 
        start=start, 
        num=num,
        single_displ_plot=False,
        # output_path=os.path.join(folder, "mean_displ_list.pkl")
    )

    displ_vector_mag_list_path = os.path.join(folder, "displ_vector_mag_list.pkl")
    with open(displ_vector_mag_list_path, "wb") as pkl_file:
        pickle.dump(displ_vector_mag_list, pkl_file)
        pkl_file.close()
    print("saving mean_displ_list to:", displ_vector_mag_list_path)


    plots(stats=mean_displ_list, filename="mean_displ_hist.png")
    plots(stats=displ_vector_mag_list, filename="displ_vector_mag_hist.png")
