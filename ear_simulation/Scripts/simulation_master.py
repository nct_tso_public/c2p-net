import utils
import subprocess
import os


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description="Simulation middle ear st")
    parser.add_argument("config", type=str, help="Path to the configuration file")
    args = parser.parse_args()
    config_path = args.config

    config = utils.read_yml(config_path)

    # print("Generating {:d} random meshes")

    for idx in range(config["num"]):
        print("\n===================================== Generating num {} ======================================".format(idx))
        a = [config["blender"]["exe_path"]]
        a += [config["blender"]["scene_path"]]
        a += ["--background"]
        a += ["-noaudio"]
        a += ["--python"]
        a += [config["blender"]["python_path"]]
        a += ["--"]
        a += ["--output_path"]
        a += [os.path.join(config["output"]["folder"], "{:06d}".format(idx), "intra_surface.stl")]
        a += ["--def_ops_path"]
        a += [config["def_ops_path"]]
        # a += ["--"]
        # a += ["--num"]
        # a += [str(args.num)]
        # a += ["--start_num"]
        # a += [str(args.start_num)]
        # a += ["--outdir"]
        # a += [args.outdir]
        print("Running Blender: " + " ".join(a))
        p = subprocess.call(a)

    