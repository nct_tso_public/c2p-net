import traceback
import bpy
import mathutils
import sys, os
cur_dir = os.path.dirname(bpy.data.filepath)
print(cur_dir)
sys.path.append(os.path.join(cur_dir, "Scripts"))
import utils
import random, math, datetime


def create_armature(init_bone_name=""):
    try:
        bpy.ops.object.mode_set(mode='OBJECT')
        bpy.ops.object.armature_add(enter_editmode=False, align='WORLD', location=(0, 0, 0), scale=(1, 1, 1))

        bpy.context.view_layer.objects.active = bpy.data.objects["Armature"]
        if init_bone_name and "Bone" in bpy.data.objects["Armature"].data.bones:
            bpy.data.objects["Armature"].data.bones["Bone"].select = True
            # init_bone_name = "bone_renamed"
            print("rename first bone to:", init_bone_name)
            bpy.ops.object.mode_set(mode='POSE')
            bone = bpy.context.object.pose.bones["Bone"]
            bone.name = init_bone_name

    except Exception as e:
        traceback.print_exc()



def set_bone(bone_name, armature_name="Armature", head=None, tail=None):
    try:
        print("reset head or tail of bone:{} for armature:{}".format(bone_name, armature_name))
        bpy.ops.object.mode_set(mode='OBJECT')
        bpy.ops.object.select_all(action='DESELECT')
        bpy.context.view_layer.objects.active = bpy.data.objects[armature_name]
        bpy.ops.object.mode_set(mode='EDIT')
        print(bpy.data.objects[armature_name].data.edit_bones.keys())
        bone_edit = bpy.data.objects[armature_name].data.edit_bones[bone_name]
        
        if head:
            print("original head", bone_edit.head)
            bone_edit.head = mathutils.Vector(head)
            print("head after update", bone_edit.head)
        if tail:
            print("original tail", bone_edit.tail)
            bone_edit.tail = mathutils.Vector(tail)
            print("tail after update", bone_edit.tail)
    except Exception as e:
        traceback.print_exc()




def AddBonesAtVertices(length, use_normals):
    objects = bpy.context.view_layer.objects
    obj = objects.active
    if not obj or obj.type != 'MESH':
        return

    points = []
    normals = []
    data = []
    counter = 0
    for v in obj.data.vertices:
        print(counter)
        p = obj.matrix_world @ v.co
        target = v.normal @ obj.matrix_world
        dir = target - p
        dir.normalize()
        dir = dir * length
        n = p + dir * (-1)
        points.append(p)
        if not use_normals:
            n = mathutils.Vector((p[0], p[1], p[2] + length))
        normals.append(n)
        data.append([p, n])
        if counter > 10:
            break
        counter += 1

    amt = bpy.data.armatures.new(obj.name + "_vBones")
    rig = bpy.data.objects.new(obj.name + '_vRig', amt)
    
    bpy.context.collection.objects.link(rig)
    objects.active = rig

    bpy.ops.object.editmode_toggle()
    for i, l in enumerate(zip(points, normals)):        
        bone = amt.edit_bones.new(str(i))
        bone.head = l[0]
        bone.tail = l[1]
    bpy.ops.object.editmode_toggle()



def create_armature_for_ear():
    points_head = []
    points_tail = []

    structure_list = ["malleus", "incus", "stape"]
    for obj_name in structure_list:
        obj = bpy.data.objects[obj_name]

        for bone_name in ["bone_head", "bone_tail"]:
            # vg_selected = obj.vertex_groups["bone_head"]
            vg_selected = obj.vertex_groups[bone_name]
            vg_idx = vg_selected.index
            # print(vg_idx)
            length = 1
            for v in obj.data.vertices:
                # if len(v.groups) > 0:
                #     print(v.groups, len(v.groups))
                if vg_idx in [ vg.group for vg in v.groups ]:
                    p = obj.matrix_world @ v.co
                    target = v.normal @ obj.matrix_world
                    dir = target - p
                    dir.normalize()
                    dir = dir * length
                    n = p + dir * (-1)
                    # points.append(p)
                    if bone_name == "bone_head":
                        points_head.append(p)
                    elif bone_name == "bone_tail":
                        points_tail.append(p)


    rig_name = "ear_rig_{}"
    counter = 0
    while rig_name.format(counter) in bpy.data.objects.keys():
        counter += 1
    rig_name = rig_name.format(counter)

    amt_name = "ear_bones_{}"
    counter = 0
    while amt_name.format(counter) in bpy.data.objects.keys():
        counter += 1
    amt_name = amt_name.format(counter)

    amt = bpy.data.armatures.new(amt_name)
    rig = bpy.data.objects.new(rig_name, amt)
    print("create new armature:", amt_name)

    bpy.context.collection.objects.link(rig)
    bpy.context.view_layer.objects.active = rig

    bone_list = []
    bpy.ops.object.editmode_toggle()
    for i, l in enumerate(zip(points_head, points_tail)):        
        bone = amt.edit_bones.new(str(i))
        bone.head = l[0]
        bone.tail = l[1]
        bone_list.append(bone)
        print("create bone named {} for {}".format(i, structure_list[i]))
    # bpy.ops.object.editmode_toggle()


    # set bone parents
    print(amt.edit_bones.keys())
    amt.edit_bones['1'].parent = amt.edit_bones['0']
    amt.edit_bones['2'].parent = amt.edit_bones['1']

    for idx_o, obj_name in enumerate(["malleus", "incus", "stape", "membrane"]):
        if idx_o == 3:
            idx_o = 0
        bpy.ops.object.mode_set(mode='OBJECT')
        bpy.ops.object.select_all(action='DESELECT')
        obj = bpy.data.objects[obj_name]
        obj.select_set(True)
        rig.select_set(True)
        bpy.context.view_layer.objects.active = rig 

        bpy.ops.object.posemode_toggle()
        # bone_list[2].select_set(True)
        bone_pose_parent = rig.pose.bones[idx_o]
        bone_pose_parent.bone.select = True
        # bpy.context.active_pose_bone = bone_pose_parent
        bpy.context.object.data.bones.active = bone_pose_parent.bone
        bpy.ops.object.parent_set(type='BONE', keep_transform=False)
    return rig_name


def test_get_bone_head_tail_vg():
    for obj_name in ["malleus", "incus", "stape"]:
        print(obj_name)
        obj = bpy.data.objects[obj_name]
        for bone_name in ["bone_head", "bone_tail"]:
            # vg_selected = obj.vertex_groups["bone_head"]
            vg_selected = obj.vertex_groups[bone_name]
            vg_idx = vg_selected.index
            print(vg_idx)
            length = 1
            for v in obj.data.vertices:
                if vg_idx in [ vg.group for vg in v.groups ]:
                    print(v.co)


def transform_bones(armature_name="Armature", bname=None, ang_range=10):
    try: 
        if bname:
            bone_name = bname
        else:
            bone_name = "incus"
        # print(bone_name, "will be deformed...")
        bpy.context.view_layer.objects.active = bpy.data.objects[armature_name]
        bpy.data.objects[armature_name].data.bones[bone_name].select = True
        bpy.ops.object.mode_set(mode='POSE')
        bone_to_select = bpy.context.object.pose.bones[bone_name]


        pb = bone_to_select
        bone_to_select.rotation_mode = 'XYZ'
        angle_list = [
            random.random() * ang_range - (ang_range / 2), 
            random.random() * ang_range - (ang_range / 2), 
            random.random() * ang_range - (ang_range / 2),
            ]
        print("rotation angle:", angle_list)
        pb.rotation_euler.rotate_axis("X", math.radians(angle_list[0])) 
        pb.rotation_euler.rotate_axis("Y", math.radians(angle_list[1])) 
        pb.rotation_euler.rotate_axis("Z", math.radians(angle_list[2])) 

    except Exception as e:
        traceback.print_exc()





if __name__ == "__main__":
    # create_armature(init_bone_name="bone_new_name")
    # set_bone(bone_name="malleus", head=[0, 1, 0], tail=[4, 5, 6])
    # set_bone(bone_name="stapes", head=[0, 1, 0], tail=[3, 2, 5])
    # get_pivots()
    # adjust_bones()
    # AddBonesAtVertices(length=1, use_normals=True)
    # adjust_bone_1()
    
    amt_name = create_armature_for_ear()
    # transform_bones(armature_name=amt_name, bname="0")
    # transform_bones(armature_name=amt_name, bname="1")
    # transform_bones(armature_name=amt_name, bname="2")
    # utils.export_to_stl()
    # test_get_bone_head_tail_vg()

