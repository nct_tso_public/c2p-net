import bpy
import os
import sys


if __name__ == "__main__":
    import argparse    
    parser = argparse.ArgumentParser(description="Simluation pipeline for middle ear stucture")
    parser.add_argument("--folder", type=str, required=True, help="Folder where samples lie")
    parser.add_argument("--start", type=int,  default=0, required=False, help="Starting index of samples")
    parser.add_argument("--num", type=int,  default=1, required=False, help="The number of samples to be processed")

    argv = sys.argv
    if "--" in argv:
        argv = argv[argv.index("--") + 1:]
    else:
        argv = []

    args = parser.parse_args(argv)

    folder = args.folder
    start = args.start
    num = args.num

    objs = bpy.data.objects
    for obj in objs:
        print(obj.name)
        objs.remove(obj, do_unlink=True)

    for idx in range(start, start + num):
        print("=======================generating normals for sample: {:06d}====================".format(idx))
        cur_dir = os.path.join(folder, "{:06d}".format(idx))
        intra_surface_path = os.path.join(cur_dir, "intra_surface.stl")
        bpy.ops.import_mesh.stl(filepath=intra_surface_path, )
        obj_name = "intra_surface"
        objs = bpy.data.objects
        print("before deleting:")
        for obj in objs:
            print(obj.name)

        bpy.data.objects[obj_name].select_set(True)
        output_path = os.path.join(cur_dir, "intra_surface_with_normals.ply")
        bpy.ops.export_mesh.ply(filepath=output_path, check_existing=False, filter_glob="*.ply", use_normals=True, use_mesh_modifiers=True, axis_forward='Y', axis_up='Z', global_scale=1.0)

        objs.remove(objs[obj_name], do_unlink=True)
        print("after deleteing:")
        for obj in objs:
            print(obj.name)
        


