import os, bpy, sys
import types

cur_dir = os.path.dirname(bpy.data.filepath)
sys.path.append(os.path.join(cur_dir, "Scripts"))

import utils
try:
    import Scripts.non_rigid as non_rigid
    import Scripts.rigid as rigid
except:
    import non_rigid
    import rigid
# ...so we need to reload our submodule(s) using importlib
import importlib
importlib.reload(non_rigid)
importlib.reload(rigid)
importlib.reload(utils)



if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description="Simluation pipeline for middle ear stucture")
    parser.add_argument("--output_path", type=str,  default="", required=False, help="The number of samples to be generated")
    parser.add_argument("--def_ops_path", type=str,  default="", required=False, help="The number of samples to be generated")

    argv = sys.argv
    if "--" in argv:
        argv = argv[argv.index("--") + 1:]
    else:
        argv = []

    args = parser.parse_args(argv)
    output_path = args.output_path
    print("output_path:", output_path)

    deformation_ops_path = args.def_ops_path
    deformation_ops = utils.read_yml(deformation_ops_path)

    # non-rigid deformation
    print("==================non-rigid deformation:")
    non_rigid.deform_non_rigid(deformation_ops)
    non_rigid.apply_modifier()
    non_rigid.clear_parent()

    # rigid deformation
    print("==================rigid deformation:")
    amt_name = rigid.create_armature_for_ear()
    for bone_idx in range(3):
        print("deform {} with maximal angle {}".format(deformation_ops["rigid"]["bone_idx"][bone_idx], deformation_ops["rigid"]["ang_range"][deformation_ops["rigid"]["bone_idx"][bone_idx]] ))
        if deformation_ops["rigid"]["ang_range"][deformation_ops["rigid"]["bone_idx"][bone_idx]] > 0:
            rigid.transform_bones(armature_name=amt_name, bname="{}".format(bone_idx), ang_range=deformation_ops["rigid"]["ang_range"][deformation_ops["rigid"]["bone_idx"][bone_idx]])

    utils.record_coord(output_path=os.path.join(os.path.dirname(output_path), "coord.json"))
    utils.export_to_stl(output_path=output_path)

