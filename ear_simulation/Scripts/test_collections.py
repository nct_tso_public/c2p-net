import bpy



def get_collections():
    collections = bpy.data.collections
    print(collections["Collection"])
    print(collections[0].name, len(collections))

    collections_layer = bpy.context.view_layer.layer_collection
    # print(collections_layer, len(collections_layer))
    print(collections_layer.children["Collection"])


def remove_collection():
    print("removing collection")


def find_collcetion_recursive(layerColl, collection_name):
    # def recurLayerCollection(layerColl, collName):
    # layerColl = bpy.context.view_layer.layer_collection
    found = None
    if (layerColl.name == collection_name):
        return layerColl
    for layer in layerColl.children:
        found = find_collcetion_recursive(layer, collection_name)
        if found:
            return found


def set_active_collection(collection_name):
    coll = find_collcetion_recursive(bpy.context.view_layer.layer_collection, collection_name)
    print(coll)
    pre = bpy.context.area.type
    print(pre)
    # print(bpy.context.area.ui_type)
    bpy.context.view_layer.objects.active = bpy.data.objects["Lattice_incus"]
    bpy.context.area.type = "OUTLINER"
    print("current active layer collection:", bpy.context.view_layer.active_layer_collection.name)
    collection_to_be_copied = bpy.context.view_layer.layer_collection.children[collection_name]
    bpy.context.view_layer.active_layer_collection = collection_to_be_copied
    print("new active layer collection:", bpy.context.view_layer.active_layer_collection.name)
    print(bpy.context.collection)
    bpy.ops.outliner.collection_duplicate()
    bpy.context.area.type = pre


def duplicate_collection(collection_name):

    # bpy.context.view_layer.active_layer_collection = bpy.context.view_layer.layer_collection
    # bpy.context.view_layer.collections.active = bpy.data.collections[collection_name]
    print(bpy.context.window_manager.windows[0])
    for window in bpy.context.window_manager.windows:
        screen = window.screen
        print(screen.areas)
        for area in screen.areas:
            print(area.type)
            if area.type == 'OUTLINER':
                collection_to_be_copied = bpy.context.view_layer.layer_collection.children[collection_name]
                bpy.context.view_layer.active_layer_collection = collection_to_be_copied
                override = {'window': window, 'screen': screen, 'area': area}
                bpy.ops.outliner.collection_duplicate(override)
                break
    # bpy.ops.outliner.item_activate
    


def get_collection_parent(collection_name, master_collection):
    for coll in bpy.data.collections:
        if collection_name in coll.children.keys():
            return coll
    return master_collection

def duplicate_collection_objects(original_copy_objectmap, collection, collection_new, suffix_XP):
    for obj in collection.objects:
        obj_new = obj.copy()
        obj_new.data = obj.data.copy()  # Omit if you want a linked object.
        obj_new.name = obj_new.name[:-4] + suffix_XP  # minus 4 digits on the right + suffix
        collection_new.objects.link(obj_new)
        original_copy_objectmap[obj] = obj_new

def parent_objects(original_copy_objectmap):
    for orig, copy in original_copy_objectmap.items():
        orig_parent = orig.parent
        if not (orig_parent is None):
            savedState = copy.matrix_world
            copy.parent = original_copy_objectmap[orig_parent]
            copy.matrix_world = savedState

def duplicate_collection_hierarchy(original_copy_objectmap, collection, collection_new, suffix_draft, suffix_XP):
    for coll in collection.children:
        basename = collection_basename(coll.name, suffix_draft)
        curIterationColl = bpy.data.collections.new(f"{basename}{suffix_XP}")
        collection_new.children.link(curIterationColl)

        duplicate_collection_objects(original_copy_objectmap, coll, curIterationColl, suffix_XP)

        duplicate_collection_hierarchy(original_copy_objectmap, coll, curIterationColl, suffix_draft, suffix_XP)

def collection_basename(collection_name, suffix_draft):
    basename = ''
    draft_ind = collection_name.find(suffix_draft)
    if draft_ind != -1:
        basename = collection_name[:draft_ind]
    else:
        basename = collection_name

    return basename

def duplicate_collection(suffix_draft, suffix_XP, collection, master_collection):

    collection_name = collection.name

    basename = collection_basename(collection_name, suffix_draft)

    collection_new = bpy.data.collections.new(f"{basename}{suffix_XP}")

    # link new collection to the same parent as original collection
    parent_coll = get_collection_parent(collection_name, master_collection)
    parent_coll.children.link(collection_new)

    original_copy_objectmap = {}

    # duplicate first-level collection objects
    duplicate_collection_objects(original_copy_objectmap, collection, collection_new, suffix_XP)
    # duplicate collection hierarchy
    duplicate_collection_hierarchy(original_copy_objectmap, collection, collection_new, suffix_draft, suffix_XP)

    parent_objects(original_copy_objectmap)



def iter_collection(collection):
    for col in collection.children:
        if col.children:
            print(col.children)
        print(col)



if __name__ == "__main__":
    # get_collections()
    
    # duplicate_collection("Collection")
    # set_active_collection("Collection.001")
    # duplicate_collection(
    #     suffix_draft="001",
    #     suffix_XP="test",
    #     collection=bpy.data.collections[0],
    #     # master_collection=bpy.data.collections[0],
    #     master_collection=bpy.context.scene.collection,
    # )

    iter_collection(collection=bpy.context.scene.collection)


