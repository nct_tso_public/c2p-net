import bpy
import os, sys
cur_dir = os.path.dirname(bpy.data.filepath)
sys.path.append(os.path.join(cur_dir, "Scripts"))

import utils
# ...so we need to reload our submodule(s) using importlib
import importlib
importlib.reload(utils)


def deform_lattice(lattice_name, ops_conf):

    lattice_current = bpy.data.objects[lattice_name]

    utils.deselect_all()

    lattice_current.select_set(True, view_layer=bpy.context.view_layer)
    bpy.context.view_layer.objects.active = lattice_current
    bpy.ops.object.mode_set( mode = 'EDIT' )

    vg = lattice_current.vertex_groups[ops_conf["vertex_group"]]
    bpy.context.object.vertex_groups.active_index = vg.index
    print("selecting")
    bpy.ops.object.vertex_group_select()


    p_selected_list = []
    for p in lattice_current.data.points:
        if p.select:
            p_selected_list.append(p)
            # print(p.co, p.co_deform, p.co - p.co_deform)
        
    # deform points using co_deform:
    # vector_deform = mathutils.Vector((1, 0, 0))
    # for p in p_selected_list:
    #     p.co_deform += vector_deform
    #     print(p.co, p.co_deform, p.co - p.co_deform)

    # deform points using bpy.ops
    if ops_conf["ops"] == "transform":
        print("transform selected vetices")
        bpy.ops.transform.translate( 
            value = utils.random_vector(range_list=ops_conf["range"]),
            orient_axis_ortho='Y', 
            orient_type='LOCAL', 
            orient_matrix_type='LOCAL', constraint_axis=(False, True, False), mirror=False, use_proportional_edit=False, 
            proportional_edit_falloff='SMOOTH', proportional_size=1, use_proportional_connected=False, use_proportional_projected=False)
    elif ops_conf["ops"] == "scale":
        print("scale selected vetices")
        # range = ops_conf["range"]["X"]
        bpy.ops.transform.resize(
            # value=(2, 2, 1.1), 
            value = utils.random_vector(range_list=ops_conf["range"]),
            orient_type='LOCAL', 
            orient_matrix_type='LOCAL', constraint_axis=(True, False, False), mirror=False, use_proportional_edit=False, 
            proportional_edit_falloff='SMOOTH', proportional_size=1, use_proportional_connected=False, use_proportional_projected=False)

    # time.sleep(2) 
    # print("deselecting")
    bpy.ops.object.vertex_group_deselect()
 


def deform_non_rigid(deformation_ops):
    print("non-rigid deformation for all structures")
    for k_structure in deformation_ops["non-rigid"].keys():
        print("------------deform {}:".format(k_structure))
        ops_conf_list = deformation_ops["non-rigid"][k_structure]
        print(ops_conf_list["ops_list"])
        for ops in ops_conf_list["ops_list"]:
            deform_lattice(lattice_name=ops_conf_list["lattice_name"], ops_conf=ops)


def apply_modifier():
    print("applying modifier...")
    # bpy.ops.object.mode_set(mode='OBJECT')
    # obj_name = "stape"
    for obj_name in ["stape", "incus", "malleus", "membrane"]:
        bpy.ops.object.mode_set(mode='OBJECT')
        bpy.ops.object.select_all(action='DESELECT')
        obj = bpy.data.objects[obj_name]
        obj.select_set(True)
        bpy.context.view_layer.objects.active = obj 
        bpy.ops.object.mode_set(mode='OBJECT')
        print(obj_name, obj.modifiers, len(obj.modifiers), obj.modifiers[0].name)
        bpy.ops.object.modifier_apply(modifier = obj.modifiers[0].name)


def clear_parent():
    print("clearing parent...")
    bpy.ops.object.mode_set(mode='OBJECT')
    # obj_name = "stape"
    for obj_name in ["stape", "malleus", "incus"]:
        bpy.ops.object.select_all(action='DESELECT')
        obj = bpy.data.objects[obj_name]
        obj.select_set(True)
        bpy.context.view_layer.objects.active = obj 
        bpy.ops.object.parent_clear(type='CLEAR_KEEP_TRANSFORM')




if __name__ == "__main__":
    for obj in bpy.data.objects:
        print(obj.name)

    deformation_ops_path = "deformation_ops.yml"
    deformation_ops = utils.read_yml(deformation_ops_path)

    deform_non_rigid(deformation_ops)
    apply_modifier()
    clear_parent()

    # utils.export_to_stl()
 
    # test translation
    # ops_conf_list = deformation_ops["non-rigid"]["incus"]
    # print(ops_conf_list["ops_list"][0])
    # deform_lattice(lattice_name=ops_conf_list["lattice_name"], ops_conf=ops_conf_list["ops_list"][0])

    # test scale
    # ops_conf_list = deformation_ops["non-rigid"]["membrane_malleus"]
    # print(ops_conf_list["ops_list"][1])
    # deform_lattice(lattice_name=ops_conf_list["lattice_name"], ops_conf=ops_conf_list["ops_list"][1])

    # test a whole structure
    # ops_conf_list = deformation_ops["non-rigid"]["incus"]
    # print(ops_conf_list["ops_list"])
    # for ops in ops_conf_list["ops_list"]:
    #     deform_lattice(lattice_name=ops_conf_list["lattice_name"], ops_conf=ops)

