import traceback
import bpy
import yaml
import random
import os
import datetime


def find_context_for_ops(ops):
    #all the area types except 'EMPTY' from blender.org/api/blender_python_api_current/bpy.types.Area.html#bpy.types.Area.type
    # types = {'VIEW_3D', 'TIMELINE', 'GRAPH_EDITOR', 'DOPESHEET_EDITOR', 'NLA_EDITOR', 'IMAGE_EDITOR', 'SEQUENCE_EDITOR', 'CLIP_EDITOR', 'TEXT_EDITOR', 'NODE_EDITOR', 'LOGIC_EDITOR', 'PROPERTIES', 'OUTLINER', 'USER_PREFERENCES', 'INFO', 'FILE_BROWSER', 'CONSOLE'}
    types = ['EMPTY', 'VIEW_3D', 'IMAGE_EDITOR', 'NODE_EDITOR', 'SEQUENCE_EDITOR', 'CLIP_EDITOR', 'DOPESHEET_EDITOR', 'GRAPH_EDITOR', 'NLA_EDITOR', 'TEXT_EDITOR', 'CONSOLE', 'INFO', 'TOPBAR', 'STATUSBAR', 'OUTLINER', 'PROPERTIES', 'FILE_BROWSER', 'SPREADSHEET', 'PREFERENCES']
    #save the current area
    area = bpy.context.area.type

    #try each type
    for type in types:
        #set the context
        bpy.context.area.type = type

        #print out context where operator works (change the ops below to check a different operator)
        if bpy.ops.armature.select_all.poll():
            print(type)
        else:
            print("no")

    #leave the context where it was
    bpy.context.area.type = area



def deselect_all():
    try:
        print("deselecting all...")
        # pre = bpy.context.area.type
        # print(pre)
        # # print(bpy.context.area.ui_type)
        # bpy.context.view_layer.objects.active = bpy.data.objects["Lattice_incus"]
        # bpy.context.area.type = "VIEW_3D"
        bpy.ops.object.mode_set(mode='OBJECT')
        bpy.ops.object.select_all(action='DESELECT')
        
        bpy.ops.object.mode_set(mode="EDIT") #Activating Edit mode
        # bpy.context.view_layer.objects.active = bpy.data.objects["Lattice_incus"]
        bpy.ops.object.vertex_group_deselect()
        # bpy.ops.mesh.select_all(action = 'DESELECT')

        bpy.context.view_layer.objects.active = None
        # for b in bpy.data.objects["Armature"].data.bones:
        #     print(b.name)
        # for pb in bpy.context.object.pose.bones:
        #     print(pb.name)
        # print(bpy.data.objects["Armature"].data.bones)
        # bpy.ops.mesh.select_all(action='SELECT')
        # bpy.context.area.type = pre
        # bone_name_list = ["malleus", "incus", "stapes"]
        # bpy.ops.object.mode_set(mode='POSE')
        # bpy.context.view_layer.objects.active = None
        # for bone_name in bone_name_list:
        #     bpy.data.objects["Armature"].data.bones[bone_name].select = False
    except Exception as e:
        traceback.print_exc()
    # finally:
    #     bpy.context.area.type = pre



def get_vetex_group(vertex_group_name, obj=None, obj_name=None):
    try:
        if not obj:
            if not obj_name:
                print("please give at least obj or obj_name")
                return None
            else:
                obj = bpy.data.objects[obj_name]
        print(obj.name)
        bpy.ops.object.mode_set( mode = 'EDIT' )
        print(obj.vertex_groups)

        vg_selected = obj.vertex_groups[vertex_group_name]
        vg_idx = vg_selected.index

        
        vs = [ v for v in obj.data.vertices if vg_idx in [ vg.group for vg in v.groups ] ]
        print("number of points:", len(vs))
        for v in vs:
            # print(v.index)
            print(v.co)
        world_matrix = obj.matrix_world
        vs_co_world = [world_matrix @ v.co for v in vs ]
        print(vs_co_world)
        return vg_selected, vs, vs_co_world
    except Exception as e:
        traceback.print_exc()


def set_vertex_parent(obj, parent_vertex):
    try:
        pass
    except Exception as e:
        traceback.print_exc()



def get_lattice_vertex_group(vertex_group_name, lattice=None, lattice_name=None):
    if not lattice:
        if not lattice_name:
            print("please give at least obj or obj_name")
            return None
        else:
            lattice = bpy.data.objects[lattice_name]
    print(lattice.name)
    bpy.ops.object.mode_set( mode = 'EDIT' )

    print(lattice.data.points)

    for p in lattice.data.points:
        print(p.co, p.select)

    vg_selected = lattice.vertex_groups[vertex_group_name]



def read_yml(path):
    with open(path, "r") as stream:
        try:
            deformation_ops = yaml.safe_load(stream)
            # print(yaml.safe_load(stream))
            return deformation_ops
        except yaml.YAMLError as exc:
            print(exc)
            return None



def random_vector(range_list):
    print(range_list)
    v = []
    for k in ("X", "Y", "Z"):
        r = range_list[k]
        if r[0] == r[1]:
            v.append(r[0])
        else: 
            v.append(random.uniform(r[0], r[1]))
    print("random transform vecter:", v)
    return v




def export_to_stl(output_folder=None, filename=None, output_path=None):

    # deselect all meshes
    bpy.ops.object.mode_set(mode='OBJECT')
    bpy.ops.object.select_all(action='DESELECT')

    for obj_name in ["membrane", "malleus", "incus", "stape"]:
        obj = bpy.data.objects[obj_name]
        obj.select_set(True)
        obj.to_mesh(preserve_all_data_layers=True)
        # bpy.context.view_layer.objects.active = obj 

    # export object with its name as file name
    if output_path:
        output_path = output_path 
    else:
        if not output_folder:
            output_folder = "/home/liupeng/ceph_home/Code/Blender_proj/MiddleEar/outputs"
        if not filename:
            filename = "deform_{}.stl".format(datetime.datetime.now().strftime("%Y_%d_%m_%H_%M_%S"))
        output_path = os.path.join(output_folder, filename)    

    if not os.path.exists(os.path.dirname(output_path)):
        print("creating path")
        os.makedirs(os.path.dirname(output_path))
    #bpy.context.active_object = object
    print("output deformed ear mesh to:", output_path)
    bpy.ops.export_mesh.stl(filepath=output_path)



def export_to_stl_separate(output_folder=None,):

    # deselect all meshes
    bpy.ops.object.mode_set(mode='OBJECT')
    bpy.ops.object.select_all(action='DESELECT')

    for obj_name in ["membrane", "malleus", "incus", "stape"]:
        obj = bpy.data.objects[obj_name]
        obj.select_set(True)
        obj.to_mesh(preserve_all_data_layers=True)
        # bpy.context.view_layer.objects.active = obj 

        if not os.path.exists(output_folder):
            print("creating path")
            os.makedirs(output_folder)
        #bpy.context.active_object = object
        print("output deformed ear mesh to:", os.path.join(output_folder, obj_name))
        # bpy.ops.export_mesh.stl(filepath=output_path)



def record_coord(output_path=None):
    obj_names = ["membrane", "malleus", "incus", "stape"]
    coord_list = {}
    for obj_name in obj_names:
        obj = bpy.data.objects[obj_name]
        coord_list[obj_name] = [v.co @ obj.matrix_world for v in obj.data.vertices]
    # print(coord_list)
    if output_path:
        # with open("/mnt/ceph/tco/TCO-Staff/Homes/liupeng/data/D2EAR_data/SimulationData/coord_original.json", "w") as f:
        if not os.path.exists(os.path.dirname(output_path)):
            os.makedirs(os.path.dirname(output_path))
        with open(output_path, "w") as f:
            import json
            json.dump(coord_list, f, default=lambda v: list(v))
    return coord_list




if __name__ == "__main__":
    # find_context_for_ops(bpy.ops.object.select_all)
    # find_context_for_ops(bpy.ops.mesh.select_all)
    # get_vetex_group(vertex_group_name="bone_head", obj_name="membrane", )
    # get_vetex_group(vertex_group_name="bone_tail", obj_name="malleus", )
    # get_vetex_group(vertex_group_name="bone_tail", obj_name="incus", )
    # get_vetex_group(vertex_group_name="bone_tail", obj_name="stape", )
    # get_vetex_group(vertex_group_name="bone_head", obj_name="stape", )
    # get_lattice_vertex_group(vertex_group_name="vertex_full", lattice_name="Lattice_membrane_malleus")
    # deselect_all()
    # deformation_ops_path = "/home/liupeng/ceph_home/Code/Blender_proj/MiddleEar/Scripts/deformation_ops.yml"
    deformation_ops_path = "/home/liupeng/ceph_home/Code/Blender_proj/MiddleEar/Configs/deformation_ops/small_rigid.yml"
    deformation_ops = read_yml(deformation_ops_path)
    print(deformation_ops["rigid"]["bone_idx"]["0"])
    # print(deformation_ops)
    # export_to_stl(output_path="/mnt/ceph/tco/TCO-Staff/Homes/liupeng/data/D2EAR_data/SimulationData/mean_model_vertex_reordered.stl")
    # record_coord(output_path="/mnt/ceph/tco/TCO-Staff/Homes/liupeng/data/D2EAR_data/SimulationData/coord_original.json")
