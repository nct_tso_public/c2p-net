"""
Reads all .stl files from segments folder of a sample
concatenation > centering > point sampling
Usage: python read_ear_data_real [PATH TO REAL EAR DATA]
[PATH TO REAL EAR DATA]/*/segments/*.stl
"""

from glob import glob
import numpy as np
import trimesh as trm
import sys
import open3d as o3d
from pickle import load, dump

data_path = sys.argv[1]

# Load an intra sample for centering information
intra = trm.load('ear_dataset/004991/intra_surface.stl').vertices

for sample in glob(f'{data_path}/*/segments'):
    sample_name = sample.split('\\')[1]
    print(sample_name)
    points = []
    for i in glob(f'{sample}/*.stl'):
        obj = trm.load(i)
        points.extend(obj.vertices)
    points = np.unique(np.array(points), axis=0)

    index = np.random.choice(np.arange(len(points)), (5995))
    
    points_filtered = points[index]
    center = (np.median(points_filtered, axis=0) - np.median(intra,axis=0))
    points_filtered = points_filtered - center
    landmarks_intra = {
        path.replace('\\','/').split('/')[-1].split('.')[0]:np.asarray(o3d.io.read_point_cloud(path).points) - center
        for path in glob(f'ear_data_real/{sample_name}/landmarks/*.ply')
    }
    with open(f'oct_outputs/{sample_name}_lndmrks.pkl', 'wb') as f:
        dump(landmarks_intra, f)

    if 'l' in sample_name:
        points_filtered = points_filtered * [-1, 1, 1]
        if sample_name == '2020-008-l':
            points_filtered /= 20
    np.save(f'oct_outputs/{sample_name}.npy', points_filtered)

    oct_pcd = o3d.geometry.PointCloud()
    oct_pcd.points = o3d.utility.Vector3dVector(np.array(points_filtered))
    o3d.io.write_point_cloud(f'oct_outputs/{sample_name}.ply', oct_pcd)