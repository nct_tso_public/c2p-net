"""
Reads all .stl files from segments folder of a sample
concatenation > centering > point sampling
Usage: python read_ear_data_real [PATH TO DATA]
[PATH TO DATA]/*.stl (glob can be used, i.e. path/to/*/)
"""

from glob import glob
import numpy as np
import trimesh as trm
import sys
import open3d as o3d
from pickle import load, dump

data_path = sys.argv[1]
source_path = sys.argv[2]

# Load an intra sample for centering information

def norm(points, mean=0, std=1):
    return (points-mean)/std

pre = trm.load(source_path)
pre_points = np.asarray(pre.vertices)

std = np.std(pre_points)

for u, sample in enumerate(glob(data_path)):
    sample_name = (sample.split('\\')[-1]).split('.')[0]
    sample_name = f'{sample_name}_{u}'
    obj = trm.load(sample)
    points = np.unique(np.array(obj.vertices), axis=0)

    index = np.random.choice(np.arange(len(points)), (5995))
    points_filtered = points[index]

    center = (np.median(points_filtered, axis=0) - np.median(pre_points, axis=0))
    points_filtered = norm(points_filtered, center, std)

    np.save(f'custom_data/{sample_name}.npy', points_filtered)

    oct_pcd = o3d.geometry.PointCloud()
    oct_pcd.points = o3d.utility.Vector3dVector(np.array(points_filtered))
    o3d.io.write_point_cloud(f'custom_data/{sample_name}.ply', oct_pcd)
pre.vertices = norm(pre_points, std=std)
pre.export('custom_data/pre_surface.stl')

with open('custom_data/metadata.pkl', 'rb') as f:
    f.write({'mean': np.median(pre_points, axis=0), 'std': std})